
# ChessManager

- Adres: https://chessmanager123.herokuapp.com/
- HMAC kalkulator: https://www.devglan.com/online-tools/hmac-sha256-online
- OpenAPI: OpenAPI.yaml (proszę otworzyć plik z tego repozytorium, na hostingu z jakiegoś powodu OpenApi szwankuje)
- Basic Auth credentials: user - pba_user, pass - 123456 **Basic cGJhX3VzZXI6MTIzNDU2**
- HMAC key: 123456

# Co robi aplikacja?

- Wyświetla ranking graczy według największego ELO (endpoint: /ranking)
- Wyświetla historię meczów według daty (endpoint: /matchHistory)
- Możliwość dodania nowej partii - na tej podstawie obliczane jest nowe ELO, a ranking aktualizowany (endpoint: /addMatch)

# Diagram sekwencji

![image info](./sequence.png)