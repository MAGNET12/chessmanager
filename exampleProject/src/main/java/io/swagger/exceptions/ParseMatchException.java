package io.swagger.exceptions;

public class ParseMatchException extends RuntimeException {
    public ParseMatchException() {
        super();
    }

    public ParseMatchException(String message) {
        super(message);
    }

    public ParseMatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseMatchException(Throwable cause) {
        super(cause);
    }

    protected ParseMatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
