package io.swagger.exceptions;

public class NoPlayerExistsException extends RuntimeException {
    public NoPlayerExistsException() {
        super();
    }

    public NoPlayerExistsException(String message) {
        super(message);
    }

    public NoPlayerExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoPlayerExistsException(Throwable cause) {
        super(cause);
    }

    protected NoPlayerExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
