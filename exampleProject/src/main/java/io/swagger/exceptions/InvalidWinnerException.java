package io.swagger.exceptions;

public class InvalidWinnerException extends RuntimeException {
    public InvalidWinnerException() {
        super();
    }

    public InvalidWinnerException(String message) {
        super(message);
    }

    public InvalidWinnerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidWinnerException(Throwable cause) {
        super(cause);
    }

    protected InvalidWinnerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
