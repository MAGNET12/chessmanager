package io.swagger.exceptions;

public class MatchExistsException extends RuntimeException {
    public MatchExistsException() {
        super();
    }

    public MatchExistsException(String message) {
        super(message);
    }

    public MatchExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public MatchExistsException(Throwable cause) {
        super(cause);
    }

    protected MatchExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
