package io.swagger.HelperFunctions;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class HelperFunctions {

    // checks if provided credentials match "pba_user" and "123456"
    public static boolean CheckCredentials(String b64Credentials) {
        b64Credentials = b64Credentials.replace("Basic ", "");
        byte[] credentialsDecoded = Base64.getDecoder().decode(b64Credentials);
        String credentials = new String(credentialsDecoded, StandardCharsets.UTF_8);

        String[] fields = credentials.split(":", 2);

        if (!fields[0].equals("pba_user") || !fields[1].equals("123456")) {
            return false;
        }

        return true;
    }
}
