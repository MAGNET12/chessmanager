package io.swagger.Repo;

import io.swagger.model.Player;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Repository
public class PlayerRepo {
    private List<Player> listOfPlayers;
    private int idCounter;

    public PlayerRepo() {
        this.idCounter = 1;
        this.listOfPlayers = new ArrayList<Player>();
    }

    public void addNewPlayer(Player newMatch) {
        newMatch.setId(this.idCounter);
        this.idCounter += 1;
        this.listOfPlayers.add(newMatch);

        sortPlayers();
    }

    public void sortPlayers() {
        Collections.sort(this.listOfPlayers, (lhs, rhs) -> {
            return rhs.getElo() - lhs.getElo();
        });
    }

    public List<Player> getAllPlayers() {
        return this.listOfPlayers;
    }

    public boolean playerExists(final Player newPlayer) {
//         return this.listOfMatches.stream().filter(match -> match.getCity().equals(newMatch.getCity())).findFirst().isPresent();
        return this.listOfPlayers.stream().anyMatch(match -> (match.getName().equals(newPlayer.getName())));
    }

    public Player findPlayerByName(String playerName) {
        return this.listOfPlayers.stream().filter(player -> player.getName().equals(playerName)).findAny().orElse(null);
    }

    public Player findPlayerById(Integer id) {
        return this.listOfPlayers.stream().filter(player -> player.getId() == id).findAny().orElse(null);
    }
}
