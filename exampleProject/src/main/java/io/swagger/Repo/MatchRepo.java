package io.swagger.Repo;

import io.swagger.model.Match;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.lang.Math.abs;

@Repository
public class MatchRepo {
    private List<Match> listOfMatches;
    private int idCounter;

    public MatchRepo() {
        this.idCounter = 1;
        this.listOfMatches = new ArrayList<Match>();
    }

    public void addNewMatch(Match newMatch) {
        newMatch.setId(this.idCounter);
        this.idCounter += 1;
        this.listOfMatches.add(newMatch);

        sortMatches();
    }

    public void sortMatches() {
        Collections.sort(this.listOfMatches, (lhs, rhs) -> {
            Integer YEAR = 0;
            Integer MONTH = 1;
            Integer DAY = 2;

            List<Integer> dateLhs = lhs.parseDate(lhs.getDate());
            List<Integer> dateRhs = rhs.parseDate(rhs.getDate());

            // prepare -1 0 1 values for year, month and day
            Integer yearBigger = dateLhs.get(YEAR) - dateRhs.get(YEAR);
            if (yearBigger != 0) {
                yearBigger /= abs(yearBigger) * -1;
            }
            Integer monthBigger = dateLhs.get(MONTH) - dateRhs.get(MONTH);
            if (monthBigger != 0) {
                monthBigger /= abs(monthBigger) * -1;
            }
            Integer dayBigger = dateLhs.get(DAY) - dateRhs.get(DAY);
            if (dayBigger != 0) {
                dayBigger /= abs(dayBigger) * -1;
            }

            if (yearBigger == 0) {
                if (monthBigger == 0) {
                    return dayBigger;
                }
                else {
                    return monthBigger;
                }
            }
            else {
                return yearBigger;
            }
        });
    }

    public List<Match> getAllMatches() {
        return this.listOfMatches;
    }

    public boolean matchExists(final Match newMatch) {
        return this.listOfMatches.stream().anyMatch(match -> (match.getCity().equals(newMatch.getCity()) && match.getDate().equals(newMatch.getDate()) && match.getWinner() == newMatch.getWinner()));
    }
}
