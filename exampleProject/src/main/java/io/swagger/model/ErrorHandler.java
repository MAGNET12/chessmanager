package io.swagger.model;

import io.swagger.exceptions.*;
import io.swagger.model.ResponseHeader;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.UUID;

@ControllerAdvice
public class ErrorHandler {
    @ExceptionHandler(InvalidCredentialsException.class)
    public ResponseEntity<Error> handleInvalidCredentialsException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("401");
        error.message("Invalid credentials provided!");
        return new ResponseEntity<Error>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(InvalidDateException.class)
    public ResponseEntity<Error> handleInvalidDateException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("406");
        error.message("Invalid date format! Should be: YYYY-MM-DD");
        return new ResponseEntity<Error>(error, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(ParseMatchException.class)
    public ResponseEntity<Error> handleParseMatchException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("400");
        error.message("Invalid Match object provided in the body!");
        return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(MatchExistsException.class)
    public ResponseEntity<Error> handleMatchExistsException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("406");
        error.message("Match with provided fields already exists!");
        return new ResponseEntity<Error>(error, HttpStatus.NOT_ACCEPTABLE);
    }
    @ExceptionHandler(NoPlayerExistsException.class)
    public ResponseEntity<Error> handleNoPlayerExistsException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("406");
        error.message("No such player exists!");
        return new ResponseEntity<Error>(error, HttpStatus.NOT_ACCEPTABLE);
    }
    @ExceptionHandler(InvalidWinnerException.class)
    public ResponseEntity<Error> handleInvalidWinnerException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("406");
        error.message("Incorrect winner ID! Should be either white's ID, black's ID or 0 (draw)");
        return new ResponseEntity<Error>(error, HttpStatus.NOT_ACCEPTABLE);
    }
    @ExceptionHandler(InvalidSignatureException.class)
    public ResponseEntity<Error> handleInvalidSignatureException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("400");
        error.message("Incorrect HMAC signature!");
        return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
    }
}
