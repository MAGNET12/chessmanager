package io.swagger.model;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.exceptions.InvalidDateException;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Match
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-17T21:44:19.090Z[GMT]")
@NoArgsConstructor
@AllArgsConstructor
public class Match   {
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("playerWhiteID")
  private Integer playerWhiteID = null;

  @JsonProperty("playerBlackID")
  private Integer playerBlackID = null;

  @JsonProperty("winner")
  private Integer winner = null;

  @JsonProperty("date")
  private String date = null;

  @JsonProperty("city")
  private String city = null;

  public Match(Integer playerWhiteID, Integer playerBlackID, Integer winner, String date, String city) {
    this.playerWhiteID = playerWhiteID;
    this.playerBlackID = playerBlackID;
    this.winner = winner;
    this.date = date;
    this.city = city;
  }

  public List<Integer> parseDate(String date) {
    if (!validateDate(date)) {
      return Arrays.asList(-1, -1, -1);
    }

    int year;
    int month;
    int day;

    List<String> items = Arrays.asList(date.split("\\s*-\\s*"));
    try {
      year = Integer.parseInt(items.get(0));
      month = Integer.parseInt(items.get(1));
      day = Integer.parseInt(items.get(2));
    } catch (NumberFormatException e) {
      throw new InvalidDateException("");
    }
    return Arrays.asList(year, month, day);
  }

  public boolean validateDate(String date) {
    List<String> items = Arrays.asList(date.split("\\s*-\\s*"));

    if (items.size() != 3) {
      throw new InvalidDateException("");
    }

    int year;
    int month;
    int day;

    try {
      year = Integer.parseInt(items.get(0));
      month = Integer.parseInt(items.get(1));
      day = Integer.parseInt(items.get(2));
    } catch (NumberFormatException e) {
      throw new InvalidDateException("");
    }

    if (year < 1000 || year > Calendar.getInstance().get(Calendar.YEAR)) {
      throw new InvalidDateException("Invalid year!");
    }

    if (month < 1 || month > 12) {
      throw new InvalidDateException("Invalid month! Should be between 1-12");
    }

    if (day < 1 || day > 31) {
      throw new InvalidDateException("Invalid day! Should be between 1-31");
    }

    if (month == 4 || month == 6 || month == 9 || month == 11) {
      if (day == 31) {
        throw new InvalidDateException("The month you provided doesn't have 31 days!");
      }
    }

    if (month == 2) {
      if (year % 4 == 0) {
        if (day > 29) {
          throw new InvalidDateException("February can't have more than 29 days in the leap year!");
        }
      }
      else {
        if (day > 28) {
          if (day > 29) {
            throw new InvalidDateException("February can't have more than 28 days in non-leap year!");
          }
        }
      }
    }

    return true;
  }

  public boolean areFieldsValid() {
    if (this.city == null || this.date == null || this.playerBlackID == null || this.winner == null || this.playerWhiteID == null) {
      return false;
    }
    return true;
  }

  public Match id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Match playerWhiteID(Integer playerWhiteID) {
    this.playerWhiteID = playerWhiteID;
    return this;
  }

  /**
   * Get playerWhiteID
   * @return playerWhiteID
   **/
  @Schema(description = "")
  
    public Integer getPlayerWhiteID() {
    return playerWhiteID;
  }

  public void setPlayerWhiteID(Integer playerWhiteID) {
    this.playerWhiteID = playerWhiteID;
  }

  public Match playerBlackID(Integer playerBlackID) {
    this.playerBlackID = playerBlackID;
    return this;
  }

  /**
   * Get playerBlackID
   * @return playerBlackID
   **/
  @Schema(description = "")
  
    public Integer getPlayerBlackID() {
    return playerBlackID;
  }

  public void setPlayerBlackID(Integer playerBlackID) {
    this.playerBlackID = playerBlackID;
  }

  public Match winner(Integer winner) {
    this.winner = winner;
    return this;
  }

  /**
   * Get winner
   * @return winner
   **/
  @Schema(description = "")
  
    public Integer getWinner() {
    return winner;
  }

  public void setWinner(Integer winner) {
    this.winner = winner;
  }

  public Match date(String date) {
    this.date = date;
    return this;
  }

  /**
   * Get date
   * @return date
   **/
  @Schema(description = "")
  
    public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public Match city(String city) {
    this.city = city;
    return this;
  }

  /**
   * Get city
   * @return city
   **/
  @Schema(description = "")
  
    public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Match match = (Match) o;
    return Objects.equals(this.id, match.id) &&
        Objects.equals(this.playerWhiteID, match.playerWhiteID) &&
        Objects.equals(this.playerBlackID, match.playerBlackID) &&
        Objects.equals(this.winner, match.winner) &&
        Objects.equals(this.date, match.date) &&
        Objects.equals(this.city, match.city);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, playerWhiteID, playerBlackID, winner, date, city);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Match {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    playerWhiteID: ").append(toIndentedString(playerWhiteID)).append("\n");
    sb.append("    playerBlackID: ").append(toIndentedString(playerBlackID)).append("\n");
    sb.append("    winner: ").append(toIndentedString(winner)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
