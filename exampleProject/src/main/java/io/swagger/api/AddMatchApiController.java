package io.swagger.api;

import com.fasterxml.jackson.databind.util.JSONPObject;
import io.swagger.HelperFunctions.HelperFunctions;
import io.swagger.Repo.MatchRepo;
import io.swagger.Repo.PlayerRepo;
import io.swagger.exceptions.*;
import io.swagger.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-17T21:44:19.090Z[GMT]")
@RestController
public class AddMatchApiController implements AddMatchApi {

    private static final Logger log = LoggerFactory.getLogger(AddMatchApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    public static MatchRepo matchRepo;
    public static PlayerRepo playerRepo;

    private static final char[] HEX_ARRAY = "0123456789abcdef".toCharArray();

    @org.springframework.beans.factory.annotation.Autowired
    public AddMatchApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        playerRepo = new PlayerRepo();
        playerRepo.addNewPlayer(new Player("Magnus Carlsen", 2865));
        playerRepo.addNewPlayer(new Player("Anish Giri", 2772));
        playerRepo.addNewPlayer(new Player("Ian Nepomniachtchi", 2773));
        playerRepo.addNewPlayer(new Player("Jan-Krzysztof Duda", 2760));
        playerRepo.addNewPlayer(new Player("Fabiano Caruana", 2792));
        playerRepo.addNewPlayer(new Player("Daniil Dubov", 2720));


        matchRepo = new MatchRepo();
        matchRepo.addNewMatch(new Match(playerRepo.findPlayerByName("Magnus Carlsen").getId(),
                playerRepo.findPlayerByName("Anish Giri").getId(),
                playerRepo.findPlayerByName("Magnus Carlsen").getId(), // winner
            "2021-03-01", "Reykjavik, Iceland"));

        matchRepo.addNewMatch(new Match(playerRepo.findPlayerByName("Ian Nepomniachtchi").getId(),
                playerRepo.findPlayerByName("Magnus Carlsen").getId(),
                playerRepo.findPlayerByName("Magnus Carlsen").getId(), // winner
                "2021-12-15", "Dubai, UAE"));

        matchRepo.addNewMatch(new Match(playerRepo.findPlayerByName("Jan-Krzysztof Duda").getId(),
                playerRepo.findPlayerByName("Magnus Carlsen").getId(),
                playerRepo.findPlayerByName("Jan-Krzysztof Duda").getId(), // winner
                "2020-10-10", "Stavanger, Norway"));

        matchRepo.addNewMatch(new Match(playerRepo.findPlayerByName("Fabiano Caruana").getId(),
                playerRepo.findPlayerByName("Anish Giri").getId(),
                0, // winner - draw
                "2021-12-29", "Warsaw, Poland"));

    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public ResponseEntity<Response> postAddMatch(@Parameter(in = ParameterIn.HEADER, description = "BasicAuth credentials" ,schema=@Schema()) @RequestHeader("X-HMAC-SIGNATURE") String signature, @RequestHeader(value="Authorization", required=false) String authorization,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody String body) throws NoSuchAlgorithmException, InvalidKeyException {
        String accept = request.getHeader("Accept");

        boolean ret = HelperFunctions.CheckCredentials(authorization);
        if (ret == false) {
            throw new InvalidCredentialsException("");
        }

        // validate signature
        String key = "123456";
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
        mac.init(signingKey);
        String generatedSignature = bytesToHex(mac.doFinal(body.getBytes(StandardCharsets.UTF_8)));
        System.out.println(body);
        System.out.println(generatedSignature);

        if (generatedSignature.equals(signature) == false) {
            throw new InvalidSignatureException("");
        }

        JSONObject json = null;
        JSONObject matchJson = null;

        try {
            json = new JSONObject(body);
            matchJson = json.getJSONObject("Match");
        }
        catch(JSONException e) {
            throw new ParseMatchException("");
        }


        Match match = null;

        try {
            match = new ObjectMapper().readValue(matchJson.toString(), Match.class);
        }
        catch(IOException e) {
            throw new ParseMatchException("");
        }

        if (!match.validateDate(match.getDate())) {
            throw new InvalidDateException("");
        }

        if (matchRepo.matchExists(match)) {
            throw new MatchExistsException("Date: " + match.getDate() + ", city: " + match.getCity() + ", winner: " + match.getWinner());
        }

        if (match.getWinner() != 0 && match.getWinner() != match.getPlayerWhiteID() && match.getWinner() != match.getPlayerBlackID()) {
            throw new InvalidWinnerException("");
        }

        Player whitePlayer = playerRepo.findPlayerById(match.getPlayerWhiteID());
        Player blackPlayer = playerRepo.findPlayerById(match.getPlayerBlackID());

        if (whitePlayer == null) {
            throw new NoPlayerExistsException("White player, id: " + match.getPlayerWhiteID());
        }

        if (blackPlayer == null) {
            throw new NoPlayerExistsException("Black player, id: " + match.getPlayerBlackID());
        }

        matchRepo.addNewMatch(match);

        // update players' ELO
        Double scoreWhite = 0.5; // draw
        Double scoreBlack = 0.5; // draw

        if (match.getWinner() == match.getPlayerWhiteID()) { // white wins
            scoreWhite = 1.0;
            scoreBlack = 0.0;
        }
        else if (match.getWinner() == match.getPlayerBlackID()) { // black wins
            scoreWhite = 0.0;
            scoreBlack = 1.0;
        }

        Double rWhite = Math.pow(10, Double.valueOf(whitePlayer.getElo())/400.0);
        Double rBlack = Math.pow(10, Double.valueOf(blackPlayer.getElo())/400.0);

        Double eWhite = rWhite / (rWhite + rBlack);
        Double eBlack = rBlack / (rWhite + rBlack);

        // impact factor
        Double K = 32.0;

        Integer eloWhite = (int) Math.round(Double.valueOf(whitePlayer.getElo()) + K * (scoreWhite - eWhite));
        Integer eloBlack = (int) Math.round(Double.valueOf(blackPlayer.getElo()) + K * (scoreBlack - eBlack));

        String res = whitePlayer.getId() + " vs " + blackPlayer.getId() + ". ";
        res += "Winner: " + match.getWinner() + ". ";
        res += "ELO White: " + whitePlayer.getElo() + " -> " + eloWhite + ". ";
        res += "ELO Black: " + blackPlayer.getElo() + " -> " + eloBlack + ". ";

        System.out.println(res);

        Response r = new Response();
        r.setMessage(res);

        whitePlayer.setElo(eloWhite);
        blackPlayer.setElo(eloBlack);

        return ResponseEntity.ok().body(r);
    }

}
