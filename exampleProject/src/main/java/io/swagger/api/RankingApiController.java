package io.swagger.api;

import io.swagger.HelperFunctions.HelperFunctions;
import io.swagger.exceptions.InvalidCredentialsException;
import io.swagger.model.InlineResponse200;
import io.swagger.model.InlineResponse2001;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.Match;
import io.swagger.model.Player;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static io.swagger.api.AddMatchApiController.matchRepo;
import static io.swagger.api.AddMatchApiController.playerRepo;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-17T21:44:19.090Z[GMT]")
@RestController
public class RankingApiController implements RankingApi {

    private static final Logger log = LoggerFactory.getLogger(RankingApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public RankingApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<InlineResponse2001> getRanking(@Parameter(in = ParameterIn.HEADER, description = "BasicAuth credentials" ,schema=@Schema()) @RequestHeader(value="Authorization", required=false) String authorization) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<InlineResponse2001>(objectMapper.readValue("{\n  \"Players\" : [ {\n    \"name\" : \"name\",\n    \"elo\" : 6,\n    \"id\" : 0\n  }, {\n    \"name\" : \"name\",\n    \"elo\" : 6,\n    \"id\" : 0\n  } ]\n}", InlineResponse2001.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse2001>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        boolean ret = HelperFunctions.CheckCredentials(authorization);
        if (ret == false) {
            throw new InvalidCredentialsException("");
        }

        playerRepo.sortPlayers();

        List<Player> playerList = playerRepo.getAllPlayers();

        InlineResponse2001 res = new InlineResponse2001();
        res.players(playerList);

        return new ResponseEntity<InlineResponse2001>(res, HttpStatus.OK);
    }

}
