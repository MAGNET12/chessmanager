package io.swagger.api;

import io.swagger.HelperFunctions.HelperFunctions;
import io.swagger.exceptions.InvalidCredentialsException;
import io.swagger.model.InlineResponse200;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.Match;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

import static io.swagger.api.AddMatchApiController.matchRepo;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-17T21:44:19.090Z[GMT]")
@RestController
public class MatchHistoryApiController implements MatchHistoryApi {

    private static final Logger log = LoggerFactory.getLogger(MatchHistoryApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public MatchHistoryApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<InlineResponse200> getMatchHistory(@Parameter(in = ParameterIn.HEADER, description = "BasicAuth credentials" ,schema=@Schema()) @RequestHeader(value="Authorization", required=true) String authorization) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<InlineResponse200>(objectMapper.readValue("{\n  \"Matches\" : [ {\n    \"date\" : \"date\",\n    \"winner\" : 5,\n    \"city\" : \"city\",\n    \"id\" : 0,\n    \"playerBlackID\" : 1,\n    \"playerWhiteID\" : 6\n  }, {\n    \"date\" : \"date\",\n    \"winner\" : 5,\n    \"city\" : \"city\",\n    \"id\" : 0,\n    \"playerBlackID\" : 1,\n    \"playerWhiteID\" : 6\n  } ]\n}", InlineResponse200.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse200>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        boolean ret = HelperFunctions.CheckCredentials(authorization);
        if (ret == false) {
            throw new InvalidCredentialsException("");
        }

        matchRepo.sortMatches();

        List<Match> matchList = matchRepo.getAllMatches();

//        return new ResponseEntity<InlineResponse200>(HttpStatus.NOT_IMPLEMENTED);

        InlineResponse200 res = new InlineResponse200();
        res.matches(matchList);

        return new ResponseEntity<InlineResponse200>(res, HttpStatus.OK);
    }

}
